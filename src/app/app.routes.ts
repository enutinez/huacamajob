import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Component } from '@angular/core';


//Component
import { HomeComponent } from './components/home/home.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { FollowUsComponent } from './components/follow-us/follow-us.component';
import { LogInComponent } from './components/log-in/log-in.component'


const routes:Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'about-us', component: AboutUsComponent},
    {path: 'follow-us', component: FollowUsComponent},
    {path: 'log-in', component: LogInComponent},
    { path: '**', pathMatch: 'full', redirectTo : 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(routes, {useHash:true});
