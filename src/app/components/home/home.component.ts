import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  constructor() { }

  ngOnInit(): void {

    //Capturar todo el tamaño de la pantalla
    $(document).ready(function(){
      var height = $(window).height();
      $('#full').height(height);
    });
  }

}
