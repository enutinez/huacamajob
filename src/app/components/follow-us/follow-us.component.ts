import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
@Component({
  selector: 'app-follow-us',
  templateUrl: './follow-us.component.html',
  styleUrls: ['./follow-us.component.css']
})
export class FollowUsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    $(document).ready(()=>{
      var height = $(window).height();

      $('#follow').height(height);
    })
  }

}
